# Gulp + Stylus + Postcss

## [Gulp.js](https://gulpjs.com/)
>O gulp é um kit de ferramentas para automatizar tarefas dolorosas ou demoradas em seu fluxo de trabalho de desenvolvimento, para que você possa parar de mexer e construir algo.
>**Instalação:** npm install gulp --save-dev

>### [gulp-stylus](https://www.npmjs.com/package/gulp-stylus)
>>Usado para compilar arquivos .styl
>
>>**Instalação:** npm install gulp-stylus --save-dev

>### [gulp-uglify](https://www.npmjs.com/package/gulp-uglify)
>>Usado para minificar arquivos .js
>
>>**Instalação:** npm install gulp-uglify --save-dev

>### [gulp-watch](https://www.npmjs.com/package/gulp-watch)    
>>Usado para inspecionar os arquivos que estão sendo alterados e executar as funções determinadas
>
>>**Instalação:** npm install gulp-watch --save-dev

>### [gulp-rename](https://www.npmjs.com/package/gulp-rename)    
>>Usado para renomear os arquivos
>
>>**Instalação:** npm install gulp-rename --save-dev

>### [gulp-replace](https://www.npmjs.com/package/gulp-replace)    
>>Usado para substituir strings nos arquivos
>
>>**Instalação:** npm install gulp-replace --save-dev

## [Gulp-postcss](https://www.npmjs.com/package/gulp-postcss)    
>PostCSS é uma ferramenta para transformar estilos com plugins JS. Esses plugins podem deixar seu CSS, suportar variáveis ​​e mixins, transpilar futuras sintaxes CSS, imagens embutidas e muito mais.
>
>**Instalação:** npm install gulp-postcss --save-dev

>### Plugins usados com PostCss

>### [autoprefixer](https://github.com/postcss/autoprefixer)    
>>Plugin PostCSS para analisar CSS e adicionar prefixos de fornecedores a regras CSS usando valores de  Can I Use . É  recomendado pelo Google e usado no Twitter e no Taobao.
>
>>**Instalação:** npm install autoprefixer --save-dev

>### [precss](https://github.com/jonathantneal/precss)    
>>O PreCSS permite usar a marcação Sass e os recursos CSS em CSS.
>
>>**Instalação:** npm install precss --save-dev

>### [postcss-cssnext](http://cssnext.io/)    
>>PostCSS-cssnext é um plugin PostCSS que ajuda você a usar a última sintaxe CSS hoje. Ele transforma novas especificações de CSS em CSS mais compatível, portanto você não precisa esperar pelo suporte do navegador.
>
>>**Instalação:** npm install postcss-cssnext --save-dev

>### [cssnano](http://cssnano.co/)    
>>O cssnano pega seu CSS bem formatado e o executa através de muitas otimizações focadas, para garantir que o resultado final seja o menor possível para um ambiente de produção. Ou seja, minifica o css.
>
>>**Instalação:** npm install cssnano --save-dev
