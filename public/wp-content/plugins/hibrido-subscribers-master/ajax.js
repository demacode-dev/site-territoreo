jQuery(document).ready(function ($) {

    var $form = $('form[data-hibrido-subscribers-form]');

    if ( ! $form.length) {
        return;
    }

    var $button = $form.find('button[type="submit"]');
    var originalButtonText = $button.text();
    var $responseWrapper = $('[data-hibrido-subscribers-response]');

    $form.ajaxForm({
        data: {
            action: hibrido_subscribers_ajax_object.action
        },
        dataType: 'json',
        beforeSend: function () {
            $responseWrapper.removeClass('success error').html('');
            $button.text(hibrido_subscribers_ajax_object.loadingButtonText).attr('disabled', '');
        },
        complete: function () {
            $button.text(originalButtonText).removeAttr('disabled');
        },
        success: function (response) {
            if (response.success) {
                $responseWrapper.addClass('success');
            } else {
                $responseWrapper.addClass('error');
            }

            /*var emailMC = $("form[data-hibrido-subscribers-form] :input[name='email']").val();
            
            var setionMC = "us14";
            var listId = "9b3ea7c05a";
            var token = "7c3f549ffca9ef8be5277d41450fd17b-us14";

            $.ajax({
              type: "POST",
              url: "https://" + setionMC + ".api.mailchimp.com/3.0/lists/" + listId + "/members/",
              data: '{ "email_address" : "' + emailMC + '" , "status" : "subscribed"',
              headers: {
                    "Authorization": "Basic " + btoa("anystring:" + token)
              },
              success: function(data){
                alert('mc')
              },
              dataType: 'json'
            });*/

            $responseWrapper.html(response.msg);
        }
    });

});
