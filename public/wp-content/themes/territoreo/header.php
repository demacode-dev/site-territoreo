<?php  do_action('before_wphead_after_init');?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">
<head>

	<?php
	//HC::init();
	?>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0" />
	<title><?php echo wp_title();?></title>
    <!-- FONT AWESOME - ICONES PERSONALIZADOS -->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- FONTE DO SITE -->
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/header.min.css?version=1">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/footer.min.css?version=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/js/plugins/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
	<?php wp_head();?>
	
	
</head>

<body>
<div class="container-loading" id="container-loading">
	<div class="conteudo loading">
		<img id="logo" src="<?php echo get_stylesheet_directory_uri()?>/img/logo_territoreo.svg">
		<img id="gif" src="<?=get_stylesheet_directory_uri()?>/img/loading.gif">
	</div>
</div>
<header class="header">
	<div class="menu-lateral" id="menu-lateral">
		<div class="after-container">
			<div class="fechar-menu">
				<img onclick="abrirMenu()" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cancel.svg" alt="">
				<a href="/"><img class="logo" src="<?php echo get_stylesheet_directory_uri()?>/img/logo_territoreo.svg"></a>
				<div class="div-vazia"></div>
			</div>
			<div class="itens-responsivo">
				<ul>
					<li><a onclick="scrollParaElemento('home')" >Home</a></li>
					<li><a onclick="scrollParaElemento('quem-somos')">Quem somos</a></li>
					<li><a onclick="scrollParaElemento('lancamentos')">Lançamentos</a></li>
					<li><a onclick="scrollParaElemento('portifolio')">Portifólio</a></li>
					<li><a onclick="scrollParaElemento('contato')">Contato</a></li>
				</ul>
			</div>
			<div class="container-redes-pai">
				<div class="container-redes">
					<?php
						$redes = get_field('links_banner', 'header');
						$contadorRedes = count($redes);
						for($i =0; $i < $contadorRedes; $i++){
							$rede = $redes[$i];
					?>
					<a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
					<?php } ?>
				</div>
				<p id="copy">© <strong>Territoreo 2020</strong> // desenvolvido por <strong>Demacode_</strong></p>
			</div>
		</div>
	</div>
	<div class="container-padrao">
		<div class="container-logo">
			<a href="/"><img src="<?= get_stylesheet_directory_uri()?>/img/logo_territoreo.svg"></a>
		</div>
		<div class="container-menu-responsivo">
			<img onclick="abrirMenu()" src="<?= get_stylesheet_directory_uri()?>/img/bars-solid.svg">
		</div>
		<div class="container-links">
			<ul>
				<li><a onclick="scrollParaElemento('home')">Home</a></li>
				<li><a onclick="scrollParaElemento('quem-somos')">Quem somos</a></li>
				<li><a onclick="scrollParaElemento('lancamentos')">Lançamentos</a></li>
				<li><a onclick="scrollParaElemento('portfolio')">Portifólio</a></li>
				<li><a onclick="scrollParaElemento('contato')">Contato</a></li>
			</ul>
		</div>
	</div>
</header>

	<main>
