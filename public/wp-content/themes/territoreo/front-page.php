<?php get_header();
    wp_enqueue_style('css_front-page', get_stylesheet_directory_uri().'/src/css/front-page.min.css?version=1', array(), null, false);
?>
    <div id="home" class="container-carrossel-pai">
        <div class="container-carrossel">
            <?php
                $carrossels = get_field('bloco_carrossel')['carrossel'];
                $contadorCarrosel = count($carrossels);
                for($i = 0; $i < $contadorCarrosel; $i++){
                    $carrossel = $carrossels[$i];
            ?>
            <div class="container-banner" style="background-image: url('<?= $carrossel['imagem_background']['url']?>')">
                <div class="container-gradient"></div>
                <div class="container-menor">
                    <h1><?= $carrossel['titulo']?><span>.</span></h1>
                    <?php if( $carrossel['botao'] ){ ?>
                        <a href="<?= $carrossel['link_botao'] ?>">
                            <div class="container-botao">
                                <p class="texto-botao"> <?= $carrossel['texto_botao'] ?> </p>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="container-arrows" <?= ($contadorCarrosel < 2) ? 'hidden' : ''?>>
            <div class="container-menor">
                <div class="prev-arrow">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-left-solid.svg">
                </div>
                <div class="next-arrow">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-right-solid.svg">
                </div>
            </div>
        </div>
        <div class="container-full">
            <div class="container-redes">
                <?php
					$redes = get_field('links_banner', 'header');
					$contadorRedes = count($redes);
					for($i =0; $i < $contadorRedes; $i++){
						$rede = $redes[$i];
				?>
				<a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="quem-somos" class="container-quem-somos" style="background-image: url('<?= get_field('bloco_quem_somos')['imagem_background']['url']?>')">
        <div class="container-medio">
            <div class="container-conteudo-pai anime">
                <div class="container-conteudo">
                    <span><?= get_field('bloco_quem_somos')['nome_do_bloco'] ?></span>
                    <h1><?= get_field('bloco_quem_somos')['titulo'] ?><span>.</span></h1>
                    <p><?= get_field('bloco_quem_somos')['texto'] ?></p>
                </div>
            </div>
            <div class="container-cards-pai">
                <div class="container-cards">
                    <div class="container-cards-linha">
                    <div class="container-linha"></div>
                    <?php 
                        $cards = get_field('bloco_quem_somos')['cards'];
                        $contadorCards = count($cards);
                        for($i = 0; $i < $contadorCards; $i++){
                            $card = $cards[$i];
                    ?>
                            <div class="container-card anime">
                                <div class="container-imagem">
                                    <img src="<?= $card['imagem']['url']?>">
                                </div>
                                <div class="container-texto">
                                    <h1><?= $card['titulo']?></h1>
                                    <p><?= $card['descricao']?></p>
                                </div>
                            </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="lancamentos" class="container-lancamentos-pai">
        <?php 
            $lancamentosArgs = array(
                'post_type' => 'lancamentos',
                'hide_empty' => false,
                'post_status' => 'publish',
                'posts_per_page' => 1,
                'orderby' => 'date',
                'order' => 'DESC'
            );
            $lancamentos = get_posts($lancamentosArgs);
            $lancamento = $lancamentos[0];
        ?>
        <div class="container-conteudo-imagem">
            <div class="container-imagem-pai">
                <div class="container-imagem">
                    <img src="<?= get_field('imagem_background', $lancamento->ID)['url']?>">
                    <div class="gradient"></div>
                </div>
            </div>
            <div class="container-medio">
                <div class="container-conteudo-pai">
                    <div class="container-conteudo anime">
                        <h2>Lançamentos</h2>
                        <h1><?= get_field('titulo', $lancamento->ID)?><span>.</span></h1>
                        <p class="texto"><?= get_field('texto', $lancamento->ID)?></p>
                        <?php
                            $descricoesLancamento = get_field('descricoes', $lancamento->ID);
                            $contadorDescricoesLancamento = count($descricoesLancamento);
                            for($j = 0; $j < $contadorDescricoesLancamento; $j++){
                                $descricaoLancamento = $descricoesLancamento[$j];
                        ?>
                        <p class="descricao"> 
                            <img src="<?= $descricaoLancamento['icone']['url']?>">
                            <?= $descricaoLancamento['descricao']?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="container-opcoes">
                    <div class="container-opcao opcao-info opcao-perspectiva anime" onclick="lancamentoController('perspectiva')">
                        <img src="<?= get_stylesheet_directory_uri()?>/img/art.svg">
                        <p>Perspectiva</p>
                    </div>
                    <div class="container-opcao opcao-info opcao-planta anime" onclick="lancamentoController('planta')">
                        <img src="<?= get_stylesheet_directory_uri()?>/img/blueprint.svg">
                        <p>Plantas</p>
                    </div>
                    <div class="container-opcao opcao-info opcao-ficha ativo anime" onclick="lancamentoController('ficha')">
                        <img src="<?= get_stylesheet_directory_uri()?>/img/planning.svg">
                        <p>Ficha Técnica</p>
                    </div>
                    <div class="container-opcao opcao-info opcao-video anime" onclick="lancamentoController('video')">
                        <img src="<?= get_stylesheet_directory_uri()?>/img/player.svg">
                        <p>Vídeo</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-pequeno info anime">
            <div class="container-video lancamento video-info">
                <?php
                    $video_url = get_field('video', $lancamento->ID);
                ?>
                <iframe src="https://www.youtube.com/embed/<?= $video_url ?>?&autoplay=1" frameborder="0" allow="accelerometer; autoplay=1; loop=1; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>                   
            <div class="container-ficha lancamento ficha-info ativo">
                <?php
                    $fichas = get_field('ficha_tecnica', $lancamento->ID);
                    $contadorFichas = count($fichas);
                    for($j = 0; $j < $contadorFichas; $j++){
                        $ficha = $fichas[$j];
                ?>
                    <p class="descricao"><strong><?= $ficha['titulo']?>: </strong><?= $ficha['descricao']?></p>
                <?php } ?>
            </div>
            <div class="container-perspectiva lancamento perspectiva-info">
                <?php
                    $perspectivaLancamento = get_field('perspectiva', $lancamento->ID);
                    if(!empty($perspectivaLancamento)){
                    $contadorPerspectivaLancamento = count($perspectivaLancamento);
                    for($j = 0; $j < $contadorPerspectivaLancamento; $j++){
                        $fotoPerspectivaLancamento = $perspectivaLancamento[$j];
                ?>
                <div class="container-imagem-pai">
                    <div class="container-border">
                        <a class="grouped_elements" rel="group1" href="<?= $fotoPerspectivaLancamento['sizes']['teste']?>">
                            <img src="<?= $fotoPerspectivaLancamento['sizes']['teste']?>">
                        </a>
                    </div>
                </div>
                <?php } }?>
            </div>
            <div class="container-planta lancamento planta-info">
                <?php
                    $plantaLancamento = get_field('planta', $lancamento->ID);
                    if(!empty($plantaLancamento)){
                        $contadorPlantaLancamento = count($plantaLancamento);
                        for($j = 0; $j < $contadorPlantaLancamento; $j++){
                            $fotoPlantaLancamento = $plantaLancamento[$j];
                ?>
                <div class="container-imagem-pai">
                    <div class="container-border">
                        <a class="grouped_elements" rel="group2" href="<?= $fotoPlantaLancamento['url']?>">
                            <img src="<?= $fotoPlantaLancamento['url']?>">
                        </a>
                    </div>
                </div>
                <?php } } ?>
            </div>
        </div>
    </div>
    <div id="portfolio" class="container-portfolio">
        <div class="container-menor anime">
            <h2><?= get_field('bloco_portfolio')['nome_do_bloco']?></h2>
            <h1><?= get_field('bloco_portfolio')['titulo']?><span>.</span></h1>
            <p><?= get_field('bloco_portfolio')['texto']?></p>
        </div>
        <div class="container-cases-pai">
            <?php 
                $casesArgs = array(
                    'post_type' => 'cases',
                    'orderby' => 'name',
                    'post_status' => 'publish',
                    'order' => 'ASC',
                    'hide_empty' => false,
                    'posts_per_page' => -1
                );
                $cases = get_posts($casesArgs);
                $contadorCases = count($cases);
                for($i = 0; $i < $contadorCases; $i++){
                    $postCase = $cases[$i];
            ?>
                <div class="container-case anime">
                    <div class="container-imagem">
                        <img src="<?= get_field('imagem_background', $postCase->ID)['url']?>">
                        <div class="container-overlay case-responsivo case-<?=$postCase->ID?>"></div>
                        <div onclick="casesController('<?=$postCase->ID?>')" class="container-ativador-responsivo"></div>
                    </div>
                    <div class="container-conteudo-pai case-responsivo case-<?=$postCase->ID?>">
                        <div class="container-conteudo">
                            <div class="container-ficha portifolio-<?=$i?> ficha-<?=$i?> ativo">
                                <h1><?= get_field('titulo', $postCase->ID)?></h1>
                                <p class="texto"><?= get_field('texto', $postCase->ID)?></p>
                                <?php
                                    $descricoes = get_field('descricoes', $postCase->ID);
                                    $contadorDescricao = count($descricoes);
                                    for($j = 0; $j < $contadorDescricao; $j++){
                                        $descricao = $descricoes[$j];
                                ?>
                                    <p class="descricao">
                                        <img src="<?= $descricao['icone']['url']?>">
                                        <?= $descricao['descricao']?>
                                    </p>
                                <?php } ?>
                            </div>
                            <div class="container-perspectiva portifolio-<?=$i?> perspectiva-<?=$i?>"">
                                <?php
                                    $perspectiva = get_field('perspectiva', $postCase->ID);
                                    if(!empty($perspectiva)){
                                        $contadorPerspectiva = count($perspectiva);
                                        for($j = 0; $j < $contadorPerspectiva; $j++){
                                            $fotoPerspectiva = $perspectiva[$j];
                                ?>
                                <div class="container-imagem-pai">
                                    <div class="container-border">
                                        <a class="grouped_elements" rel="group1<?=$i?>" href="<?= $fotoPerspectiva['url']?>">
                                            <img src="<?= $fotoPerspectiva['url']?>">
                                        </a>
                                    </div>
                                </div>
                                <?php } } ?>
                            </div>
                            <div class="container-planta portifolio-<?=$i?> planta-<?=$i?>">
                                <?php
                                    $planta = get_field('planta', $postCase->ID);
                                    if(!empty($planta)){
                                        $contadorPlanta = count($planta);
                                        for($j = 0; $j < $contadorPlanta; $j++){
                                            $fotoPlanta = $planta[$j];
                                ?>
                                <div class="container-imagem-pai">
                                    <div class="container-border">
                                        <a class="grouped_elements" rel="group2<?=$i?>" href="<?= $fotoPlanta['url']?>">
                                            <img src="<?= $fotoPlanta['url']?>">
                                        </a>
                                    </div>
                                </div>
                                <?php } }?>
                            </div>
                        </div>
                        <div class="container-opcoes">
                            <div class="container-opcao" onclick="portifolioController('perspectiva', '<?=$i?>')">
                                <p><img src="<?= get_stylesheet_directory_uri()?>/img/art.svg">Perspectiva</p>
                            </div>
                            <div class="container-opcao" onclick="portifolioController('planta', '<?=$i?>')">
                                <p><img src="<?= get_stylesheet_directory_uri()?>/img/blueprint.svg">Plantas</p>
                            </div>
                            <div class="container-opcao" onclick="portifolioController('ficha', '<?=$i?>')">
                                <p><img src="<?= get_stylesheet_directory_uri()?>/img/planning.svg"> Ficha Técnica</p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="instagram" class="container-instagram-pai">
        <div class="container-menor anime">

            <?php 

                $texto_botao = get_field('bloco_instagram')['texto_botao'];
                $link = get_field('bloco_instagram')['link_botao'];
            
            ?>    

            <h1 id="titulo">Instagram<span>.</span></h1>  

            <div class="container-instagram">
                <?= do_shortcode("[instagram-feed]")?>
            </div> 

            <a id="botao" href="<?= $link ?>" target="_blank"><p><?= $texto_botao ?></p></a>    

        </div>
    </div>                                   

    <div id="contato" class="container-contato-pai">
        <div class="container-background">
            <img src="<?= get_field('bloco_contato')['imagem_background']['url']?>">
        </div>
        <div class="container-padrao anime">
            <div class="container-contato">
                <div class="container-conteudo-pai">
                    <div class="container-conteudo">
                        <h1><?= get_field('bloco_contato')['container_contato']['titulo']?><span>.</span></h1>
                        <p><?= get_field('bloco_contato')['container_contato']['email']?></p>
                        <p><?= get_field('bloco_contato')['container_contato']['telefone']?></p>
                        <p><?= get_field('bloco_contato')['container_contato']['endereco']?></p>
                    </div>
                </div>
            </div>
            <div class="container-formulario anime">
                <div class="container-conteudo-pai">
                    <div class="container-conteudo">
                        <h1><?= get_field('bloco_contato')['container_input']['titulo']?></h1>
                        <h2><?= get_field('bloco_contato')['container_input']['subtitulo']?></h2>
                        <form method="POST" action="<?php echo admin_url('admin-ajax.php'); ?>" data-hibrido-subscribers-form>
                            <input type="text" name="nome" placeholder="Nome e Sobrenome*" required>
                            <input type="text" name="email" placeholder="Seu melhor email*" required>
                            <button type="submit">Enviar</button>
                            <span data-hibrido-subscribers-response></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($){ 
            $("a.grouped_elements").fancybox();

            $('.container-carrossel').slick({
                fade: true,
                dots: false,
                arrows: true,
                draggable: false,
                prevArrow: $('.prev-arrow'),
                nextArrow: $('.next-arrow')
            }); 
        });
    </script>
<?php get_footer(); ?>