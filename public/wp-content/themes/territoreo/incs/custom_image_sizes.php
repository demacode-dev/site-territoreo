<?php

//false = scaled
add_theme_support( 'post-thumbnails' );
add_image_size('banner_principal', 1920, 682, false);
add_image_size('logo_banner', 192, 234, true);
add_image_size('imagem_carrossel', 201, 150, true);
add_image_size('teste', 380, 380, true);


add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'size_380x380' => __( 'Quadrado médio' ),
    ) );
}