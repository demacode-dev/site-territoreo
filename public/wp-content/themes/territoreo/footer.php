	</main>

</div>
	<footer>
		<div class="container-padrao">
			<div class="container-arrow">
				<div class="up-arrow" onclick="scrollParaElemento('home')">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-up-solid.svg">
                </div>
			</div>
			<div class="container-redes">
				<?php 
					$redes = get_field('redes_sociais', 'footer');
					$contadorRedes = count($redes);
					for($i =0; $i < $contadorRedes; $i++){
						$rede = $redes[$i];
				?>
					<a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['logo_da_rede']['url']?>"></a>
				<?php } ?> 
			</div>
			<div class="container-copy">
				<p>© <strong>Territoreo 2020</strong> // desenvolvido por <strong>Demacode_</strong></p>
			</div>
		</div>
	
	</footer>
	

    <?php wp_footer();?>
</body>
</html>
