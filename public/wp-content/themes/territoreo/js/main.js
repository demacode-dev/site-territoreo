jQuery(document).ready(function($){
    $('#container-loading').fadeOut();

    $target = $('.anime'),
      animationClass = 'anime-init',
      windowHeight = $(window).height(),
	  offset = windowHeight - (windowHeight / 5);
	var root = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll() {
		var documentTop = $(document).scrollTop();
		$target.each(function() {
		if (documentTop > boxTop(this) - offset) {
			$(this).addClass(animationClass);
		}
		});
	}
	animeScroll();

	$(document).scroll(function() {
		$target = $('.anime');
		animeScroll();
	});
});

function portifolioController(container, index){
    jQuery('.portifolio-' + index).fadeOut(200);
    setTimeout(function(){
        jQuery('.' + container + '-' + index).css("display", "flex");
    }, 250);
}
function casesController(id){
	//jQuery('.case-responsivo').removeClass('ativo');
	if(jQuery('.case-' + id).hasClass('ativo')){
		jQuery('.case-' + id).removeClass('ativo');
	}else{
		jQuery('.case-' + id).addClass('ativo');
	}
	setTimeout(function(){
		
	},100)
}
function scrollParaElemento(elemento){
	document.getElementById('menu-lateral').classList.remove('mostrar-menu');
	setTimeout(function(){
		document.getElementById(elemento).scrollIntoView();
	}, 250)
    
}

function lancamentoController(container){
    jQuery('.lancamento').fadeOut(200);
    jQuery('.opcao-info').removeClass('ativo');
    jQuery('.opcao-' + container).addClass('ativo');
    setTimeout(function(){
        jQuery('.' + container + '-info').css("display", "flex");
    }, 250);
}

function filtroController() {
    var containerFiltro = document.getElementById('container-options');
    if(!containerFiltro.classList.contains('dropdown-ativo')) {
        containerFiltro.classList.add('dropdown-ativo');
    } else {
        containerFiltro.classList.remove('dropdown-ativo');
    }
}
function abrirMenu(){
	var menuLateral = document.getElementById('menu-lateral');

	if(!menuLateral.classList.contains('mostrar-menu')) {
		menuLateral.classList.add('mostrar-menu');
			
	} else {
		menuLateral.classList.remove('mostrar-menu');
	}
}

